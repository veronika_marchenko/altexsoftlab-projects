﻿using Lib_For_Complex;
using Number_calc;
using System;
using System.Collections.Generic;

namespace CalcNew
{
    class ManagerToool
    {
        private string _oper;
        bool op = false;
        Dictionary<string, double> num = new Dictionary<string, double>();
        double simple1, simple2, real1, real2, imag1, imag2;
        string choice;

        public void StartWork()
        {
            do
            {
                do
                {
                    Console.WriteLine("Complex(c)/Simple(s)?");
                    choice = Console.ReadLine();
                }
                while (choice != "c" && choice != "s");

                if (choice == "s")
                {
                    do
                    {
                        Console.WriteLine("Enter first number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out simple1));
                    do
                    {
                        Console.WriteLine("Enter second number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out simple2));


                    num.Add("fsn", simple1);
                    num.Add("ssn", simple2);


                    Calculator calculation = new Calculator();

                        do
                    {
                        Console.WriteLine("Enter operator:");
                        _oper = Console.ReadLine();

                        for (int i = 0; i < calculation.OperationAmount(); i++)
                        {
                            if (_oper.CompareTo(calculation.Operation(i)) == 0)
                                op = true;
                        }

                    }
                    while (!op);


                    Console.WriteLine($"Result: {calculation.Calc(num, _oper)}");
                    Console.WriteLine("Exit(e)/Next(n)");

                }
                else
                {
                    do
                    {
                        Console.WriteLine("Enter real part of the first number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out real1));

                    do
                    {
                        Console.WriteLine("Enter imaginary part of the first number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out imag1));

                    do
                    {
                        Console.WriteLine("Enter real part of the second number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out real2));

                    do
                    {
                        Console.WriteLine("Enter imaginary part of the second number:");
                    }
                    while (!Double.TryParse(Console.ReadLine(), out imag2));

                    num.Clear();

                    num.Add("real1", real1);
                    num.Add("imag1", imag1);
                    num.Add("real2", real2);
                    num.Add("imag2", imag2);


                    Complex_calc calculation = new Complex_calc();

                    do
                    {
                        Console.WriteLine("Enter operator:");
                        _oper = Console.ReadLine();

                        for (int i = 0; i < calculation.OperationAmount(); i++)
                        {
                            if (_oper.CompareTo(calculation.Operation(i)) == 0)
                                op = true;
                        }

                    }
                    while (!op);

                    Console.WriteLine($"Result: {calculation.Calc(num, _oper)}");
                    Console.WriteLine("Exit(e)/Next(n)");
                }
            }

            while (Console.ReadLine() != "e");
        }
    }
}
