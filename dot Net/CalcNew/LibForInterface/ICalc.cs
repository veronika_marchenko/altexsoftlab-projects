﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibForInterface
{
    public interface ICalc
    {
        string Calc(Dictionary <string, double> num, string oper);
        
        int OperationAmount();

        string Operation(int i);
    }
}
