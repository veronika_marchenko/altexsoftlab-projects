﻿using LibForInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_For_Complex
{
    public class Complex
    {
        private double real;
        private double imag;

        public Complex() { }

        public Complex(double rNum, double imNum)
        {
            this.real = rNum;
            this.imag = imNum;
        }

        public static Complex operator +(Complex firstNum, Complex secondNum)
        {
            return new Complex(firstNum.real + secondNum.real, firstNum.imag + secondNum.imag);
        }


        public static Complex operator -(Complex firstNum, Complex secondNum)
        {
            return new Complex(firstNum.real - secondNum.real, firstNum.imag - secondNum.imag);
        }

        public static Complex operator *(Complex firstNum, Complex secondNum)
        {
            return new Complex(firstNum.real * secondNum.real-firstNum.imag*secondNum.imag, firstNum.real*secondNum.imag + secondNum.real*firstNum.imag);
        }

        public static Complex operator /(Complex firstNum, Complex secondNum)
        {
            return new Complex(((firstNum.real * secondNum.real+firstNum.imag*secondNum.imag)/(Math.Pow(secondNum.real,2)+Math.Pow(secondNum.imag, 2))), (secondNum.real*firstNum.imag-secondNum.imag*firstNum.real)/(Math.Pow(secondNum.real, 2)+ Math.Pow(secondNum.imag, 2)));
        }

        public override string ToString()
        {
            if (imag > 0)
            {
                return $"{real}+j{imag}";
            }
            else if (imag<0)
            {
                return $"{real}-j{Math.Abs(imag)}";
            }
            else
            {
                return $"{real}";
            }
        }

        }
}

