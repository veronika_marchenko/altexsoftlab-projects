﻿using LibForInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib_For_Complex
{
    public class Complex_calc : ICalc
    {
        private string[] operation = new string[] { "+", "-", "*", "/" };

        public int OperationAmount()
        {
            return operation.Length;
        }

        public string Operation(int i)
        {
            return operation[i];
        }

        public string Calc(Dictionary<string, double> num, string oper)

        {
            double fr, fim, sr, sim;

            num.TryGetValue("real1", out fr);//real part of the first number
            num.TryGetValue("imag1", out fim);//imaginary part of the first number
            Complex firstNum = new Complex(fr, fim);

            num.TryGetValue("real2", out sr);
            num.TryGetValue("imag2", out sim);
            Complex secondNum = new Complex(sr, sim);

            if (operation[0] == oper)
            {return Add(firstNum, secondNum).ToString(); }
            else
            {
                if (operation[1] == oper) { return Min(firstNum, secondNum).ToString(); }
                else
                {
                    if (operation[2] == oper) { return Mul(firstNum, secondNum).ToString(); }
                    else
                    {
                        if (sr == 0 && sim == 0)
                        {
                            return "division by zero";
                        }
                        else
                        {

                            return Dev(firstNum, secondNum).ToString();
                        }
                    }
                }
            }
            
        }
        public Complex Add(Complex a, Complex b)
        {
            return a + b;
        }

        public Complex Min(Complex a, Complex b)
        {
            return a - b;
        }

        public Complex Mul(Complex a, Complex b)
        {
            return a * b;
        }

        public Complex Dev(Complex a, Complex b)
        {
            return a / b;
        }
    }

}
