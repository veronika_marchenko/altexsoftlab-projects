﻿using LibForInterface;
using System;
using System.Collections.Generic;

namespace Number_calc
{
    public class Calculator:ICalc
    {
        private string[] operation = new string[] { "+", "-", "*", "/" };

        public int OperationAmount()
        {
            return operation.Length;
        }

        public string Operation(int i) {
            return operation[i];
        }

        public string Calc(Dictionary<string, double> num, string oper)

        {
            double a, b;
            num.TryGetValue("fsn", out a);
            num.TryGetValue("ssn", out b);

            if (operation[0] == oper) { return Convert.ToString(Add(a, b)); }
            else
            {
                if (operation[1] == oper) { return Convert.ToString(Min(a, b)); }
                else
                {
                    if (operation[2] == oper) { return Convert.ToString(Mul(a, b)); }
                    else
                    {
                        if (b == 0)
                        {
                            return "division by zero";
                        }
                        else
                        {
                            return Convert.ToString((Dev(a, b)));
                        }
                    }
                }
            }
        }
            public double Add(double a, double b)
            {
                return a + b;
            }

             double Min(double a, double b)
             {
                 return a - b;
             }

             double Mul(double a, double b)
             {     
                 return a * b;
             }     

             double Dev(double a, double b)
             {
                 return a/b;
             }
        }
    } 
