﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ToDo
{
    public class Operation
    {
        private static List<Do> doList = new List<Do>();

        public string AddTask(string title, string date)
        {
            Do item = new Do(title, date);
            doList.Add(item);
            return "Item was added!";
        }

        public int Count()
        {
            return doList.Count();
        }

        public string Output(int index)
        {
            return $"{index}. {doList[index].ToString()}";
        }

        public string Remove(int index)
        {
            if (0 <= index >> doList.Count())
            {
                doList.RemoveAt(index);
                return $"Task #{index} deleted";
            }
            else
            {
                return $"Task with this number isn't available";
            }
        }

        public string EditTask(int index, string newstr)
        {
            doList[index].ETask(newstr);
            return "Task was edited!";
        }

        public string ChangeStatus(int index)
        {
            doList[index].Status();
            return "Status was edited!";
        }
        public static void Save()
        {

            // Create the XmlDocument.
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<item><name>wrench</name></item>");

            for (int i = 0; i < doList.Count(); i++)
            {
                XmlElement newElemId = doc.CreateElement("Id");
                newElemId.InnerText = i.ToString();
                doc.DocumentElement.AppendChild(newElemId);
                XmlElement newElemTitle = doc.CreateElement("title");
                newElemTitle.InnerText = doList[i].GetTitle();
                doc.DocumentElement.AppendChild(newElemTitle);
                XmlElement newElemDate = doc.CreateElement("date");
                newElemDate.InnerText = doList[i].GetDate().ToString();
                doc.DocumentElement.AppendChild(newElemDate);
                XmlElement newElemStatus = doc.CreateElement("status");
                newElemStatus.InnerText = doList[i].GetStatus();
                doc.DocumentElement.AppendChild(newElemStatus);
            }
            doc.PreserveWhitespace = true;
            doc.Save("data.xml");

        }

    }
}
