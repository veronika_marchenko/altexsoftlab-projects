﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo
{
    public class Do
    {
        private string task { get; set; }
        private DateTime date { get; set; }
        private bool status { get; set; }

        public Do() { }

        public Do(string task, string date)
        {
            this.task = task;
            this.date = Convert.ToDateTime(date);
            this.status = false;
        }

        public override string ToString()
        {
            return $"Task:{ task} Date: {date.Date} Status: {status.ToString()}";
        }

        public void ETask(string str)
        {
            this.task = str;
        }

        public void Status()
        {
            if (status)
            {
                status = false;
            }
            else
            {
                this.status = true;
            }
        }

        public DateTime GetDate()
        {
            return date;
        }

        public string GetTitle()
        {
            return task;
        }

        public string GetStatus()
        {
            return status.ToString();
        }
    }
}
