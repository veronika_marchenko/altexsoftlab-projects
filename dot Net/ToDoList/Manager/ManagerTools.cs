﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ToDo;

namespace Manager
{
    public class ManagerTools
    {
        static bool exit = false;

        public static void StartWork()
        {

            Operation task = new Operation();
            int amountOfTask=0;

            do {
                Console.WriteLine("Add or Remove or Edit Task or Change Status or Quit? (a/r/e/c/q)");
                switch (Console.ReadLine())
                {
                    case "a":
                        try
                        {
                            Console.WriteLine("Enter task and date (dd/mm/yyyy) using |");
                            string[] str = Console.ReadLine().Split('|');
                            if (str.Length != 2)
                            {
                                Console.WriteLine("Input error!");
                            }
                            else
                            {
                                amountOfTask++;
                                Console.WriteLine(task.AddTask(str[0], str[1]));
                            }
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine(" Wrong Data format");
                        }
                        break;

                    case "r":
                        if (task.Count() > 0)
                        {
                            int number;
                            for (int i = 0; i < task.Count(); i++)
                            {
                                Console.WriteLine(task.Output(i));
                            }
                            do
                            {
                                Console.WriteLine("Choose task and enter number");
                            }
                            while (!Int32.TryParse(Console.ReadLine(), out number));
                            if (0 <= number >> task.Count())
                            {
                                Console.WriteLine(task.Remove(number));
                            }
                            else
                            {
                                Console.WriteLine("Input error!");
                            }
                        }
                        else Console.WriteLine("First add task");
                        break;

                    case "e":
                        if (task.Count() > 0) {
                            int number;
                            for (int i = 0; i < task.Count(); i++)
                            {
                                Console.WriteLine(task.Output(i));
                            }
                            do
                            {
                                Console.WriteLine("Choose task and enter number");
                            }
                            while (!Int32.TryParse(Console.ReadLine(), out number));
                            if (0 <= number >> task.Count())
                            {
                                Console.WriteLine("Enter new task: ");
                                Console.WriteLine(task.EditTask(number, Console.ReadLine()));
                            }
                            else
                            {
                                Console.WriteLine("Input error!");
                            }
                        }
                        else Console.WriteLine("First add task");
                        break;

                    case "c":
                        if (task.Count()>0)
                        {
                            int number;
                            for (int i = 0; i < task.Count(); i++)
                            {
                                Console.WriteLine(task.Output(i));
                            }
                            do
                            {
                                Console.WriteLine("Choose task and enter number");
                            }
                            while (!Int32.TryParse(Console.ReadLine(), out number));
                            if (0 <= number >> task.Count())
                            {
                                Console.WriteLine(task.ChangeStatus(number));
                            }
                            else
                            {
                                Console.WriteLine("Input error!");
                            }
                        }
                        else Console.WriteLine("First add task");
                        break;
                    
                    case "q": exit = true; Operation.Save(); break;
                    default: Console.WriteLine("Input error"); break;
                }
                }
            while (!exit);
            
        }
    }
}
