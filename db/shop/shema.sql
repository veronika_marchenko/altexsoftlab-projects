CREATE TABLE [users] (
	id int NOT NULL,
	role_id int NOT NULL,
	name nvarchar(50) NOT NULL,
	phone varchar(15) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
  CONSTRAINT [PK_USERS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [roles] (
	id int NOT NULL,
  CONSTRAINT [PK_ROLES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [products] (
	id int NOT NULL,
	user_id int NOT NULL,
	title nvarchar(150) NOT NULL,
	description nvarchar(1000),
	price money NOT NULL,
  CONSTRAINT [PK_PRODUCTS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [orders] (
	id int NOT NULL,
	user_id int NOT NULL,
	adress_id int NOT NULL,
	count int NOT NULL,
  CONSTRAINT [PK_ORDERS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [order_products] (
	id int NOT NULL,
	product_id int NOT NULL,
	order_id int NOT NULL,
  CONSTRAINT [PK_ORDER_PRODUCTS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [product_comments] (
	id int NOT NULL,
	product_id int NOT NULL,
	user_id int NOT NULL,
	comment nvarchar(200),
	mark char(1) NOT NULL,
	anon bit NOT NULL DEFAULT '0',
  CONSTRAINT [PK_PRODUCT_COMMENTS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [seller_comments] (
	id int NOT NULL,
	who int NOT NULL,
	whom int NOT NULL,
	comment nvarchar(200),
	mark char(1) NOT NULL,
	anon bit NOT NULL DEFAULT '0',
  CONSTRAINT [PK_SELLER_COMMENTS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [categories] (
	id int NOT NULL,
	name nvarchar NOT NULL,
  CONSTRAINT [PK_CATEGORIES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [product_categories] (
	id int NOT NULL,
	product_id int NOT NULL,
	category_id int NOT NULL,
  CONSTRAINT [PK_PRODUCT_CATEGORIES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [adresses] (
	id int NOT NULL,
	adress nvarchar NOT NULL,
	user_id int NOT NULL,
  CONSTRAINT [PK_ADRESSES] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [cart] (
	id int NOT NULL,
	user_id int NOT NULL,
  CONSTRAINT [PK_CART] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
CREATE TABLE [cart_products] (
	id int NOT NULL,
	cart_id int NOT NULL,
	product_id int NOT NULL,
  CONSTRAINT [PK_CART_PRODUCTS] PRIMARY KEY CLUSTERED
  (
  [id] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
GO
ALTER TABLE [users] WITH CHECK ADD CONSTRAINT [users_fk0] FOREIGN KEY ([role_id]) REFERENCES [roles]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [users] CHECK CONSTRAINT [users_fk0]
GO


ALTER TABLE [products] WITH CHECK ADD CONSTRAINT [products_fk0] FOREIGN KEY ([user_id]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [products] CHECK CONSTRAINT [products_fk0]
GO

ALTER TABLE [orders] WITH CHECK ADD CONSTRAINT [orders_fk0] FOREIGN KEY ([user_id]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [orders] CHECK CONSTRAINT [orders_fk0]
GO
ALTER TABLE [orders] WITH CHECK ADD CONSTRAINT [orders_fk1] FOREIGN KEY ([adress_id]) REFERENCES [adresses]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [orders] CHECK CONSTRAINT [orders_fk1]
GO

ALTER TABLE [order_products] WITH CHECK ADD CONSTRAINT [order_products_fk0] FOREIGN KEY ([product_id]) REFERENCES [products]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [order_products] CHECK CONSTRAINT [order_products_fk0]
GO
ALTER TABLE [order_products] WITH CHECK ADD CONSTRAINT [order_products_fk1] FOREIGN KEY ([order_id]) REFERENCES [orders]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [order_products] CHECK CONSTRAINT [order_products_fk1]
GO

ALTER TABLE [product_comments] WITH CHECK ADD CONSTRAINT [product_comments_fk0] FOREIGN KEY ([product_id]) REFERENCES [products]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [product_comments] CHECK CONSTRAINT [product_comments_fk0]
GO
ALTER TABLE [product_comments] WITH CHECK ADD CONSTRAINT [product_comments_fk1] FOREIGN KEY ([user_id]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [product_comments] CHECK CONSTRAINT [product_comments_fk1]
GO

ALTER TABLE [seller_comments] WITH CHECK ADD CONSTRAINT [seller_comments_fk0] FOREIGN KEY ([who]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [seller_comments] CHECK CONSTRAINT [seller_comments_fk0]
GO
ALTER TABLE [seller_comments] WITH CHECK ADD CONSTRAINT [seller_comments_fk1] FOREIGN KEY ([whom]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [seller_comments] CHECK CONSTRAINT [seller_comments_fk1]
GO


ALTER TABLE [product_categories] WITH CHECK ADD CONSTRAINT [product_categories_fk0] FOREIGN KEY ([product_id]) REFERENCES [products]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [product_categories] CHECK CONSTRAINT [product_categories_fk0]
GO
ALTER TABLE [product_categories] WITH CHECK ADD CONSTRAINT [product_categories_fk1] FOREIGN KEY ([category_id]) REFERENCES [categories]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [product_categories] CHECK CONSTRAINT [product_categories_fk1]
GO

ALTER TABLE [adresses] WITH CHECK ADD CONSTRAINT [adresses_fk0] FOREIGN KEY ([user_id]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [adresses] CHECK CONSTRAINT [adresses_fk0]
GO

ALTER TABLE [cart] WITH CHECK ADD CONSTRAINT [cart_fk0] FOREIGN KEY ([user_id]) REFERENCES [users]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [cart] CHECK CONSTRAINT [cart_fk0]
GO

ALTER TABLE [cart_products] WITH CHECK ADD CONSTRAINT [cart_products_fk0] FOREIGN KEY ([cart_id]) REFERENCES [cart]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [cart_products] CHECK CONSTRAINT [cart_products_fk0]
GO
ALTER TABLE [cart_products] WITH CHECK ADD CONSTRAINT [cart_products_fk1] FOREIGN KEY ([product_id]) REFERENCES [products]([id])
ON UPDATE CASCADE
GO
ALTER TABLE [cart_products] CHECK CONSTRAINT [cart_products_fk1]
GO

